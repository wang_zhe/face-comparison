# -*- coding: utf-8 -*-
"""
Created on Tue Dec  7 11:53:03 2021

@author: Certi
"""

from flask import Flask
import os

#UPLOAD_FOLDER = 'static/uploads/'

app = Flask(__name__)
app.secret_key = "secret key"
#app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['UPLOAD_FOLDER'] = os.getcwd()
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
