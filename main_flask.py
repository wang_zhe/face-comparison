# -*- coding: utf-8 -*-
"""
Created on Tue Dec  7 11:45:15 2021

@author: Wang Zhe 
Flask test
"""

import os
from app import app
import urllib.request
from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename

#compare_faces.py
import cv2
import argparse
import numpy as np
from pathlib import Path

from face_compare.images import get_face
from face_compare.model import facenet_model, img_to_encoding
# include hdf5 disable function

os.environ['HDF5_DISABLE_VERSION_CHECK']='2'
app.config['UPLOAD_FOLDER'] = os.getcwd()
# load model
model = facenet_model(input_shape=(3, 96, 96))

def run(image_one, image_two, save_dest=None):
    # Load images
    face_one = get_face(cv2.imread(str(image_one), 1))
    #print(face_one)
    face_two = get_face(cv2.imread(str(image_two), 1))

    # Optionally save cropped images
    if save_dest is not None:
        print(f'Saving cropped images in {save_dest}.')
        cv2.imwrite(str(save_dest.joinpath('face_one.png')), face_one)
        cv2.imwrite(str(save_dest.joinpath('face_two.png')), face_two)

    # Calculate embedding vectors
    embedding_one = img_to_encoding(face_one, model)
    embedding_two = img_to_encoding(face_two, model)

    dist = np.linalg.norm(embedding_one - embedding_two)
    print(f'Distance between two images is {dist}')
    if dist > 0.95:
        print('These images are of two different people!')
        return 0 
    else:
        print('These images are of the same person!')
        return 1
        
        
        
# Flask web API services

# ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

# app = Flask(__name__)
# app.config['UPLOAD_FOLDER'] = os.getcwd()
# app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


# html = '''
#     <!DOCTYPE html>
#     <title>Upload File</title>
#     <h1>Photo Upload</h1>
#     <form method=post enctype=multipart/form-data>
#          <input type=file name=file>
#          <input type=submit value=upload>
#     </form>
#     '''


# def allowed_file(filename):
#     return '.' in filename and \
#            filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

    
# @app.route('/uploads/<filename>')
# def uploaded_file(filename):
#     return send_from_directory(app.config['UPLOAD_FOLDER'],
#                                filename)


# @app.route('/', methods=['GET', 'POST'])
# def upload_file():
#     if request.method == 'POST':
#         file = request.files['file']
#         if file and allowed_file(file.filename):
#             filename = secure_filename(file.filename)
#             file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
#             file_url = url_for('uploaded_file', filename=filename)
            
            
#             file_names = []
#             for file in file:
#                 if file and allowed_file(file.filename):
#                     filename = secure_filename(file.filename)
#                     file_names.append(filename)
#                     file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))   
#         imgPath1="static/uploads/ic.jpeg"
#         image_one = cv2.imread(imgPath1)
#         #image_1 = cv2.cvtColor(image_1, cv2.COLOR_BGR2RGB)
#         imgPath2 = "static/uploads/camera.jpeg"
#         image_two = cv2.imread(imgPath2)
#         #image_2 = cv2.cvtColor(image_2, cv2.COLOR_BGR2RGB)
#         return run(image_one, image_two, save_dest=None)
#             # imgPath = "abc.png"
#             # img = cv2.imread(imgPath)
#             # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
#             # results_list = inference(img, show_result=True, target_shape=(260, 260))
#             # #dic1 = 
#             # return str(inference(img, show_result=True, target_shape=(260, 260)))
#     return html



# #mask detection below



# if __name__ == "__main__":
#     # for flask 
#     # modify your ip and port below
#     app.run(host='0.0.0.0', port= 5000)
    
#     parser = argparse.ArgumentParser(description="Face Mask Detection")
#     parser.add_argument('--img-mode', type=int, default=1, help='set 1 to run on image, 0 to run on video.')
#     parser.add_argument('--img-path', type=str, help='path to your image.')
#     parser.add_argument('--video-path', type=str, default='0', help='path to your video, `0` means to use camera.')
#     # parser.add_argument('--hdf5', type=str, help='keras hdf5 file')



ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif','jfif'])

def allowed_file(filename):
 	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
 	
@app.route('/')
def upload_form():
 	return render_template('upload.html')

@app.route('/api', methods=['POST'])
def upload_image():
    if 'files[]' not in request.files:
        flash('No file part')
        return redirect(request.url)
    files = request.files.getlist('files[]')
    file_names = []
    for file in files:
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file_names.append(filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))   
    #imgPath1 = "ic.jpeg"

    #image_one = cv2.imread(imgPath1)
    image_one = "ic.jpeg"
    #image_1 = cv2.cvtColor(image_1, cv2.COLOR_BGR2RGB)
    #imgPath2 = "camera.jpeg"
    #image_two = cv2.imread(imgPath2)
    image_two = 'camera.jpg'


    #image_2 = cv2.cvtColor(image_2, cv2.COLOR_BGR2RGB)
    return str(run(image_one, image_two, save_dest=None))

		#else:
		#	flash('Allowed image types are -> png, jpg, jpeg, gif')
		#	return redirect(request.url)

    #return render_template('upload.html', filenames=file_names)

# @app.route('/display/<filename>')
# def display_image(filename):
#  	#print('display_image filename: ' + filename)
#  	#return redirect(url_for('static', filename='uploads/' + filename), code=301)
     
     
if __name__ == "__main__":
    app.run(host='0.0.0.0', port= 5000)  
